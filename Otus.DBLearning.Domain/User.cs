﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Otus.DBLearning.Domain
{
  public class User
  {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; set; }

    [Required]
    public string Login { get; set; }

    public DateTime DateFrom { get; set; }
    public DateTime? DateTo { get; set; }
  }
}