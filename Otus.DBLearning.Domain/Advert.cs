using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Otus.DBLearning.Domain
{
  public class Advert
  {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; set; }

    /// <summary>
    /// ������������� ������������ ������������.
    /// </summary>
    public int UserId { get; set; }

    /// <summary>
    /// ������������� ������������ ������������.
    /// </summary>
    public int CategoryId { get; set; }



    [Required]
    public string Title { get; set; }

    [Required]
    public string Body { get; set; }

    [ForeignKey(nameof(UserId))]
    public User Author { get; set; }

    [ForeignKey(nameof(CategoryId))]
    public Category Category { get; set; }

    public IEnumerable<Comment> Comments { get; set; }

    public AdvertMetaData AdMetadata { get; set; }
  }
}