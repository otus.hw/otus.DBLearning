using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Otus.DBLearning.Domain
{
  public class Comment
  {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; set; }
    [Required]
    public string Body { get; set; }
    public DateTime Date { get; set; }

    public int UserId { get; set; }

    public int AdvertId { get; set; }


    [ForeignKey(nameof(UserId))]
    public User Author { get; set; }

    [ForeignKey(nameof(AdvertId))]
    public Advert Advert { get; set; }
  }
}