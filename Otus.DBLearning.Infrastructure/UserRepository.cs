﻿using System;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.DBLearning.Application;
using Otus.DBLearning.Domain;
using Serilog;
using StackExchange.Redis;

namespace Otus.DBLearning.Infrastructure
{
  public class UserRepository : IUserRepository
  {
    private readonly BlogDbContext _db;
    private readonly ILogger _logger;
    private readonly IDatabase _redis;
    private readonly ISubscriber _subscriber;

    public UserRepository(BlogDbContext db, IDatabase redis, ISubscriber subscriber, ILogger logger)
    {
      _db = db;
      _redis = redis;
      _subscriber = subscriber;
      _logger = logger;
    }

    public User GetById(int id)
    {
      _logger.Information("Try Get User From Cache Redis");
      var fromCache =  GetUserFromCache(id).GetAwaiter().GetResult();
      if (fromCache != null)
      {
        _logger.Information("User From Cache Redis");
        return fromCache;
      }
      _logger.Information("User From DB");
      var user = _db.Users.FirstOrDefaultAsync(x => x.Id == id);

      _logger.Information("Add User To Cache");
      _ = AddUserToCache(user.Result);

      return user.Result;
    }

    public User GetByLogin(string login)
    {
      var user = _db.Users.Where(x => x.Login == login).FirstOrDefault();
      return user;
    }

    public User Add(User user)
    {
      _db.Users.Add(user);

      _ = _subscriber.PublishAsync("events:user:added", JsonSerializer.Serialize(user), CommandFlags.FireAndForget);

      return user;
    }

    private async Task<User> GetUserFromCache(int id)
    {
      var redisKey = GetCacheKey(id);
      string cache = await _redis.StringGetAsync(redisKey);
      _logger.Information($"RedisKey {redisKey} RedisCache {cache}");
      if (!string.IsNullOrEmpty(cache))
      {
        try
        {
          return JsonSerializer.Deserialize<User>(cache);
        }
        catch (Exception e)
        {
          _logger.Error(e, "Could not deserialize user");
        }
      }

      return null;
    }

    private async Task AddUserToCache(User user)
    {
      if (user != null)
      {
        var redisKey = GetCacheKey(user.Id);
        TimeSpan expiry = TimeSpan.FromSeconds(2);
        var j = JsonSerializer.Serialize(user);
        await _redis.StringSetAsync(redisKey, JsonSerializer.Serialize(user), expiry, flags: CommandFlags.FireAndForget);
      }
    }

    private static string GetCacheKey(int id)
    {
      return $"users:id_{id}";
    }

    public  void SaveChanges(int id)
    {
       _db.SaveChanges();
    }

    public  User Update(User user)
    {
      return _db.Update(user).Entity;
    }

    public void SaveChanges()
    {
      _db.SaveChanges();
    }
  }
}