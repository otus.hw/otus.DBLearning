﻿using Microsoft.EntityFrameworkCore;
using Otus.DBLearning.Domain;

namespace Otus.DBLearning.Infrastructure
{
  public class BlogDbContext : DbContext
  {
    private static string _connectionString;

    public BlogDbContext(DbContextOptions<BlogDbContext> options) : base(options)
    {
      Database.EnsureCreated();
    }

    public BlogDbContext()
    {

    }

    public BlogDbContext(string connectionString)
    {
      _connectionString = connectionString;
    }


    public DbSet<User> Users { get; set; }
    public DbSet<Category> Categories { get; set; }
    public DbSet<Advert> Adverts { get; set; }
    public DbSet<Comment> Comments { get; set; }

    public DbSet<AdvertMetaData> AdvertMetaData { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
      optionsBuilder.UseNpgsql(_connectionString);
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
      modelBuilder.Entity<Category>().HasData(
        new Category()
        {
          Id = 1,
          Name = "Бытовая техника",
        },
        new Category()
        {
          Id = 2,
          Name = "сад",
        }
        );

      base.OnModelCreating(modelBuilder);
    }

  }
}