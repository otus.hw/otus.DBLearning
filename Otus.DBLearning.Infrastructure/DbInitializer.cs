﻿namespace Otus.DBLearning.Infrastructure
{
  public class DbInitializer
  {
    private readonly BlogDbContext _dataContext;

    public DbInitializer(BlogDbContext dataContext)
    {
      _dataContext = dataContext;
    }

    public void Initialize()
    {
      _dataContext.Database.EnsureCreated();
    }
  }
}
