using System;
using System.Collections.Generic;
using Dapper;
using Microsoft.EntityFrameworkCore;
using Otus.DBLearning.Application;
using Otus.DBLearning.Domain;

namespace Otus.DBLearning.Infrastructure
{
  public class CategoryRepository : ICategoryRepository
  {
    private readonly BlogDbContext _db;

    public CategoryRepository(BlogDbContext db)
    {
      _db = db;
    }

    public void Add(Category category)
    {
      throw new NotImplementedException();
    }

    public Category GetById(int id)
    {
      throw new NotImplementedException();
    }

    public IEnumerable<Category> GetCategories(int skip, int take)
    {
      var dbConnection = _db.Database.GetDbConnection();

      const string sql = "SELECT * FROM Categories C LIMIT @Take OFFSET @Skip";
      return dbConnection.Query<Category>(sql);
    }

    public IEnumerable<Category> GetCategories()
    {
      var dbConnection = _db.Database.GetDbConnection();

      const string sql = "SELECT * FROM public.\"Categories\" c ";
      var t =  dbConnection.Query<Category>(sql);
      foreach (var item in t)
      {
        Console.WriteLine(item.Name);
      }
      return t;
    }

    public void SaveChanges()
    {
      throw new NotImplementedException();
    }
  }
}