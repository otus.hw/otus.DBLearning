﻿using Otus.DBLearning.Application;
using Otus.DBLearning.Domain;
using Serilog;
using System.Linq;

namespace Otus.DBLearning.Infrastructure
{
  public class AdvertMetaDataRepository : IAdvertMetaDataRepository
  {
    private readonly BlogDbContext _db;
    private readonly ILogger _logger;

    public AdvertMetaDataRepository(BlogDbContext db,  ILogger logger)
    {
      _db = db;
      _logger = logger;

    }

    public AdvertMetaData Add(AdvertMetaData advertMetaData)
    {
      _db.Add(advertMetaData);
      return advertMetaData;
    }

    public AdvertMetaData GetByAdvert(int advertId)
    {
      return _db.AdvertMetaData.Where(x => x.AdId == advertId).FirstOrDefault();
    }

    public void SaveChanges()
    {
      _db.SaveChanges();
    }

    public void Update(AdvertMetaData advertMetaData)
    {
      _db.Update(advertMetaData);
    }
  }
}
