﻿using Microsoft.EntityFrameworkCore;
using Otus.DBLearning.Application;
using Otus.DBLearning.Domain;
using Serilog;
using StackExchange.Redis;
using System.Collections.Generic;
using System.Linq;

namespace Otus.DBLearning.Infrastructure
{
  public class AdvertRepository : IAdvertRepository
  {
    private readonly BlogDbContext _db;
    private readonly ILogger _logger;
    private readonly IDatabase _redis;
    private readonly ISubscriber _subscriber;

    public AdvertRepository(BlogDbContext db,  ILogger logger)
    {
      _db = db;
      _logger = logger;

    }
    public Advert Add(Advert advert)
    {
      _db.Add(advert);
      return advert;
    }

    public IEnumerable<Advert> GetAll()
    {
      return _db.Adverts.Include(a => a.AdMetadata);
    }

    public IEnumerable<Advert> GetByCategoryId(int id)
    {
      var ads = _db.Adverts.Where(x => x.CategoryId == id && x.AdMetadata.Deleted == null).ToList();
      return _db.Adverts.Where(x => x.CategoryId == id && x.AdMetadata.Deleted == null).ToList();
    }

    public Advert GetById(int id)
    {
      var advert = _db.Adverts.Where(x => x.Id == id).FirstOrDefault();
      return advert;
    }

    public void SaveChanges()
    {
      _db.SaveChanges();
    }

    public void Update(Advert advert)
    {
      _db.Update(advert);
    }
  }
}
