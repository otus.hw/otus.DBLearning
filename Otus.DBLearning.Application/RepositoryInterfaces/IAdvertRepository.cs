﻿using Otus.DBLearning.Domain;
using System.Collections.Generic;

namespace Otus.DBLearning.Application
{
  public interface IAdvertRepository
  {
    Advert Add(Advert advert);

    void Update(Advert advert);

    IEnumerable<Advert> GetAll();

    Advert GetById(int id);

    IEnumerable<Advert> GetByCategoryId(int id);

    void SaveChanges();


  }
}
