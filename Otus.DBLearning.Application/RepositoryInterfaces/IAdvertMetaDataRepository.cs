﻿using Otus.DBLearning.Domain;

namespace Otus.DBLearning.Application
{
  public interface IAdvertMetaDataRepository
  {
    AdvertMetaData Add(AdvertMetaData advertMetaData);

    void Update(AdvertMetaData advertMetaData);

    AdvertMetaData GetByAdvert(int advertId);

    void SaveChanges();

  }
}
