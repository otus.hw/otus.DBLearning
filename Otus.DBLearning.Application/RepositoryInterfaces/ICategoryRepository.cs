using System.Collections.Generic;
using Otus.DBLearning.Domain;

namespace Otus.DBLearning.Application
{
  public interface ICategoryRepository
  {
  IEnumerable<Category> GetCategories(int skip, int take);

    IEnumerable<Category> GetCategories();

    void Add(Category category);

    Category GetById(int id);

    void SaveChanges();


  }
}