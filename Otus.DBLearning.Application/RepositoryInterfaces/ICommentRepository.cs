﻿using Otus.DBLearning.Domain;
using System.Collections.Generic;

namespace Otus.DBLearning.Application
{
  public interface ICommentRepository
  {
    void Add(Comment comment);

    IEnumerable<Comment> GetByAdvertId(int id);

    void SaveChanges();

  }
}
