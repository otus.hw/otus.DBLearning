using Otus.DBLearning.Domain;

namespace Otus.DBLearning.Application
{
  public interface IUserRepository
  {
    User GetById(int id);
    User GetByLogin(string login);
    User Add(User user);

    User Update(User user);
    void SaveChanges();
  }
}