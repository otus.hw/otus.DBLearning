﻿using Otus.DBLearning.Domain;

namespace Otus.DBLearning.Application.Services
{
  public interface ICategoryService
  {
    Category GetByName(string categoryName);
  }
}
