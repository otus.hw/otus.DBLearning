﻿using Otus.DBLearning.Domain;
using System.Collections.Generic;

namespace Otus.DBLearning.Application.Services
{
  public interface IAdvertService
  {
    Advert AddNewAdvert(Advert advert);

    Advert CloseAdvert(int id);

    IEnumerable<Advert> GetByCategory(int categoryId);

    Advert GetById(int id);

    Advert UpdateAdvert(Advert advert);

    IEnumerable<Advert> GetAllAdverts();

  }
}
