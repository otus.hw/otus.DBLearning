﻿using Otus.DBLearning.Domain;
using System;
using System.Collections.Generic;

namespace Otus.DBLearning.Application.Services.Impl
{
  public class AdvertService : IAdvertService
  {
    private readonly IAdvertRepository _advertRepository;
    private readonly IAdvertMetaDataRepository _advertMetaDataRepository;


    public AdvertService(IAdvertRepository advertRepository, IAdvertMetaDataRepository advertMetaDataRepository)
    {
      _advertRepository = advertRepository;
      _advertMetaDataRepository = advertMetaDataRepository;
    }

    public Advert AddNewAdvert(Advert advert)
    {
      AdvertMetaData advertMetaData = new AdvertMetaData {  Created = DateTime.Now };
      advert.AdMetadata = advertMetaData;

      _advertRepository.Add(advert);
      _advertRepository.SaveChanges();

      return advert;
    }

    public Advert CloseAdvert(int id)
    {
      Advert advert = _advertRepository.GetById(id);
      if (advert == null)
      {
        return null;
      }

      AdvertMetaData advertMetaData = _advertMetaDataRepository.GetByAdvert(advert.Id);
      advertMetaData.Deleted = DateTime.Now;

      _advertMetaDataRepository.Update(advertMetaData);

      _advertMetaDataRepository.SaveChanges();
      
      advert.AdMetadata = advertMetaData;
      return advert;
      
    }

    public  IEnumerable<Advert> GetByCategory(int categoryId)
    {
      return _advertRepository.GetByCategoryId(categoryId);
    }

    public Advert GetById(int id)
    {
      return _advertRepository.GetById(id);
    }

    public IEnumerable<Advert> GetAllAdverts()
    {
      return _advertRepository.GetAll();
    }

    public Advert UpdateAdvert(Advert advert)
    {

      AdvertMetaData advertMetaData = _advertMetaDataRepository.GetByAdvert(advert.Id);
      advertMetaData.Updated = DateTime.Now;
      advert.AdMetadata = advertMetaData;
      _advertRepository.Update(advert);
      _advertRepository.SaveChanges();

      return advert;
    }
  }
}
