﻿using Otus.DBLearning.Domain;
using System.Linq;

namespace Otus.DBLearning.Application.Services.Impl
{
  public class CategoryService : ICategoryService
  {
    ICategoryRepository _categoryRepository;
    public CategoryService(ICategoryRepository categoryRepository)
    {
      _categoryRepository = categoryRepository;
    }
    public Category GetByName(string categoryName)
    {
      var categories = _categoryRepository.GetCategories();
      return categories.Where(x => x.Name == categoryName).FirstOrDefault();
    }
  }
}
