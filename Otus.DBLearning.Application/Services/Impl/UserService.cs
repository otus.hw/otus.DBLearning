using System;
using Otus.DBLearning.Domain;

namespace Otus.DBLearning.Application.Services.Impl
{
  public class UserService : IUserService
  {
    private readonly IUserRepository _userRepository;

    public UserService(IUserRepository userRepository)
    {
      _userRepository = userRepository;
    }

    public User AddNewUser(User user)
    {
      if (user == null)
      {
        throw new ArgumentNullException(nameof(user));
      }

      var loginIsTaken = _userRepository
        .GetByLogin(user.Login);

      if (loginIsTaken != null)
      {
        throw new InvalidOperationException("Пользователь с таким логином уже существует");
      }
      user.DateFrom = DateTime.Now;
      user.DateTo = null;
      var adduser = _userRepository.Add(user);
      _userRepository.SaveChanges();
      return adduser;
    }

    public User CloseUser(string login)
    {
      var user =  _userRepository
        .GetByLogin(login);


      if (user == null)
      {
        return null;
      }

      user.DateTo = DateTime.Now;
      _userRepository.Update(user);
      return user;
    }

    public User GetUserById(int id)
    {
      return _userRepository.GetById(id);
    }
  }
}