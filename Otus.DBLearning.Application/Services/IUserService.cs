using Otus.DBLearning.Domain;

namespace Otus.DBLearning.Application
{
  public interface IUserService
  {
    User AddNewUser(User user);

    User GetUserById(int id);
    User CloseUser(string login);
  }
}