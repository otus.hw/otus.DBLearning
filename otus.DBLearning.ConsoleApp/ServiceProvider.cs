﻿using Otus.DBLearning.Application;
using Otus.DBLearning.Application.Services;
using Otus.DBLearning.Application.Services.Impl;
using Otus.DBLearning.Infrastructure;
using Serilog;
using Serilog.Events;
using Serilog.Sinks.Elasticsearch;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Text;

namespace otus.DBLearning.ConsoleApp
{
  public class ServiceProvider
  {
    public IUserService UserService { get; }
    public IAdvertService AdvertService { get; }

    public ICategoryService CategoryService { get; }

    public ServiceProvider(BlogDbContext blogDbContext, ILogger logger)
    {
      var redis = ConnectionMultiplexer.Connect("localhost:32775");
      var userRepository = new UserRepository(blogDbContext, redis.GetDatabase(), redis.GetSubscriber(), logger);
      UserService = new UserService(userRepository);

      var advertRepository = new AdvertRepository(blogDbContext, logger);
      var advertMetaDataRepository = new AdvertMetaDataRepository(blogDbContext,  logger);
      AdvertService = new AdvertService(advertRepository, advertMetaDataRepository);

      CategoryRepository categoryRepository = new CategoryRepository(blogDbContext);
      CategoryService = new CategoryService(categoryRepository);
    }
  }
}
