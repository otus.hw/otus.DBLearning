﻿using Microsoft.Extensions.Configuration;
using Otus.DBLearning.Infrastructure;
using Serilog;
using Serilog.Events;
using Serilog.Sinks.Elasticsearch;
using System;

namespace otus.DBLearning.ConsoleApp
{
  class Program
  {
    static void Main(string[] args)
    {

      IConfiguration Configuration = new ConfigurationBuilder().AddJsonFile("appsettings.json", optional: true, reloadOnChange: true).Build();
      string connection = Configuration.GetConnectionString("DefaultConnection");

      Log.Logger = new LoggerConfiguration()
        .MinimumLevel.Debug()
        .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
        .MinimumLevel.Override("Microsoft.AspNetCore", LogEventLevel.Warning)
        .Enrich.FromLogContext()
        .WriteTo.Console()
        .WriteTo.Elasticsearch(new ElasticsearchSinkOptions(new Uri("http://localhost:9200"))
        {
          AutoRegisterTemplate = true,
          AutoRegisterTemplateVersion = AutoRegisterTemplateVersion.ESv6
        })
        .CreateLogger();

       InitializeDb(connection);



      var dataContext = new BlogDbContext();
      var sp = new ServiceProvider(dataContext, Log.Logger);

      var test = new Test(sp, Log.Logger);
      test.Run();
    }

    static void InitializeDb(string connectionString)
    {
      using (var dataContext = new BlogDbContext(connectionString))
      {
        new DbInitializer(dataContext).Initialize();
      }
    }

  }
}
