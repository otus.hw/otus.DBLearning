﻿using Otus.DBLearning.Domain;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace otus.DBLearning.ConsoleApp
{
  public class Test
  {
    ServiceProvider ServiceProvider;
    ILogger _logger;
    public Test(ServiceProvider sp, ILogger logger)
    {
      ServiceProvider = sp;
      _logger = logger;
    }

    public Test PreInit()
    {
      return this;
    }

    public void Run()
    {
      _logger.Information($"{Environment.NewLine}Создание пользователей");
      AddNewUsers();

      _logger.Information($"{Environment.NewLine}Поиск User по Id");
      GetUserById(2);

      _logger.Information($"{Environment.NewLine}Добавление объявления");
      AddNewAdvert();

      _logger.Information($"{Environment.NewLine}поиск объявкления по категории");
      FindAdvert();

      _logger.Information($"{Environment.NewLine}редактирование объявления");
      UpdateAdvert();

      _logger.Information($"{Environment.NewLine}закрытие объявления");
      CloseAdvert();

      _logger.Information($"{Environment.NewLine}поиск открытых объявлений");
      GetOpenAdverts();

    }

    private void GetUserById(int id)
    {
      var user = ServiceProvider.UserService.GetUserById(id);
      _logger.Information($"user: {user.Id} {user.Login} {user.DateFrom} ");

      user = ServiceProvider.UserService.GetUserById(id);
      _logger.Information($"user: {user.Id} {user.Login} {user.DateFrom} ");
    }

    private void AddNewUsers()
    {
      List<User> users = new List<User>()
      {
        new User {Login = "user1" },
        new User {Login = "user2" },
        new User {Login = "user3" }
      };

      foreach (var user in users)
      {
        try
        {
          _logger.Debug($"Добавление user: {user.Login}");
          ServiceProvider.UserService.AddNewUser(user);
        }
        catch(Exception ex)
        {
          _logger.Warning(ex.Message);
        }
      }

      _logger.Information("проверка добавления существующего пользователя");

      try
      {
        var user = new User { Login = "user1" };
        _logger.Debug($"Добавление user: {user.Login}");
        ServiceProvider.UserService.AddNewUser(user);
      }
      catch (Exception ex)
      {
        _logger.Warning(ex.Message);
      }

    }


    private void AddNewAdvert()
    {
      Advert advert = new Advert()
      {
        Title = "Advert3",
        UserId = 1,
        Body = "Телевизор samsung",
        CategoryId = 1
      };

      advert = ServiceProvider.AdvertService.AddNewAdvert(advert);
      _logger.Information($"Добавили новое объявление {advert.Title} время добавления: {advert.AdMetadata.Created}");
    }

    private void FindAdvert()
    {
      _logger.Information("Поиск по категории ");

      var category = ServiceProvider.CategoryService.GetByName("Бытовая техника");
      var adverts = ServiceProvider.AdvertService.GetByCategory(category.Id);

      _logger.Information($"найдены объявления в категории {category.Name}");
      foreach (var item in adverts)
      {
        _logger.Information($"{item.Id}: {item.Title}: {item.Body}");
      }
    }

    private void UpdateAdvert()
    {
      // выборка всех обявлений
      var adverts = ServiceProvider.AdvertService.GetByCategory(1);
      // берём первый id
      var advert = adverts.First();
      advert.Body = "Изменено";
      ServiceProvider.AdvertService.UpdateAdvert(advert);

      _logger.Information($"Объявление {advert.Id}: {advert.Title}: {advert.Body} изменено {advert.AdMetadata.Updated.Value}");
      Thread.Sleep(1000);
    }

    private void CloseAdvert()
    {
      // выборка всех обявлений
      var adverts = ServiceProvider.AdvertService.GetByCategory(1);
      // берём первый id
      var advertId = adverts.First().Id;

      var advert = ServiceProvider.AdvertService.CloseAdvert(advertId);

      _logger.Information($"Закрыто объявление {advert.Id}, {advert.Title}, дата {advert.AdMetadata.Deleted}]");

      adverts = ServiceProvider.AdvertService.GetByCategory(advert.CategoryId);
      foreach (var item in adverts)
      {
        _logger.Information($"{item.Id}: {item.Title}: {item.Body}");
      }
    }

    private void GetOpenAdverts()
    {
      // Всего объявлений
      var adverts = ServiceProvider.AdvertService.GetAllAdverts();
      _logger.Information($"Всего объявлений {adverts.Count()}");
      foreach (var item in adverts)
      {
        _logger.Information($" {item.Id}: {item.Title}: {item.Body}");
      }
      // открытых объявлений
      _logger.Information($"открытых объявлений {adverts.Where(x => x.AdMetadata.Deleted == null).Count()}") ;
      foreach (var item in adverts.Where(x => x.AdMetadata.Deleted == null))
      {
        _logger.Information($"{item.Id}: {item.Title}: {item.Body}");
      }
    }
  }
}
